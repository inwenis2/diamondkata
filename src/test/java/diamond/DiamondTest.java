package diamond;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;

public class DiamondTest {

    private static ArrayList<Character> testAlphabetList = Diamond.createAlphabetArray();

    @Test
    public void shouldCreateAnUpperCaseLettersAlphabetList() {
        String testAlphabetString = "[A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Q, R, S, T, U, V, W, X, Y, Z]";

        Assert.assertEquals(testAlphabetString, testAlphabetList.toString());
    }

    @Test
    public void shouldPrintCharacterA(){

        String testUpperHalfString = "A\n";

        Assert.assertEquals(testUpperHalfString, Diamond.printDiamondUpperHalf('A'));
    }

    @Test
    public void shouldCreateAnUpperHalfOfDiamondForC(){

        String testUpperHalfString = "--A\n-B-B\nC---C\n";

        Assert.assertEquals(testUpperHalfString, Diamond.printDiamondUpperHalf('C'));
    }

    @Test
    public void shouldCreateALowerHalfOfDiamondForC(){

        String testLowerHalfString = "-B-B\n--A\n";

        Assert.assertEquals(testLowerHalfString, Diamond.printDiamondLowerHalf('C'));
    }

    @Test
    public void shouldCreateADiamondForC(){

        String testCString = "--A\n-B-B\nC---C\n-B-B\n--A\n";

        Assert.assertEquals(testCString, Diamond.createDiamond('C'));
    }

    @Test
    public void shouldCreateADiamondForE(){

        String testEString = "----A\n---B-B\n--C---C\n-D-----D\nE-------E\n-D-----D\n--C---C\n---B-B\n----A\n";

        Assert.assertEquals(testEString, Diamond.createDiamond('E'));
    }

}
